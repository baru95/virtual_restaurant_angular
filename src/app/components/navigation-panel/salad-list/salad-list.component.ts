import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/rest-api/services/product.service';
import { Product } from 'src/app/rest-api/models/product.model';

@Component({
  selector: 'app-salad-list',
  templateUrl: './salad-list.component.html',
  styleUrls: ['./salad-list.component.scss']
})
export class SaladListComponent implements OnInit {
  title = 'Salads';
  salads: Product[];

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.getSalads();
  }

  getSalads() {
    this.productService.getProductsByCategory('Salads').subscribe(
      product => (this.salads = product));
  }

}
