import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-discount',
  templateUrl: './discount.component.html',
  styleUrls: ['./discount.component.scss']
})
export class DiscountComponent {
  /** Based on the screen size, switch from standard to one column per row */
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          {
            title: '2xBigBurger',
            image:
              'https://dsmaipc1fg2ox.cloudfront.net/uploads/20160210090536/big-mac.png?410',
            description: 'Buy two BigBurgers and pay for one',
            cols: 1,
            rows: 1
          },
          {
            title: 'Quarter Pounder -20%',
            image:
              'https://dsmaipc1fg2ox.cloudfront.net/uploads/20160210091404/mcroyal.png?410',
            description: 'From today QuarterPounter will buy 20% cheaper',
            cols: 1,
            rows: 1
          },
          {
            title: '2xBigFries',
            image:
              'https://dsmaipc1fg2ox.cloudfront.net/uploads/20160229221357/frytki_srednie.png?421',
            description: 'Today we have big fries for you for half the price',
            cols: 1,
            rows: 1
          },
          {
            title: '2xCola',
            image:
              'https://dsmaipc1fg2ox.cloudfront.net/uploads/20170511122153/cola-0-4.png?421',
            description: 'Buy more for less. -30% ss',
            cols: 1,
            rows: 1
          }
        ];
      }

      return [
        {
          title: '2xBigBurger',
          image:
            'https://dsmaipc1fg2ox.cloudfront.net/uploads/20160210090536/big-mac.png?410',
          description: 'Buy two BigBurgers and pay for one',
          cols: 1,
          rows: 1
        },
        {
          title: 'Quarter Pounder -20%',
          image:
            'https://dsmaipc1fg2ox.cloudfront.net/uploads/20160210091404/mcroyal.png?410',
          description: 'From today QuarterPounter will buy 20% cheaper',
          cols: 1,
          rows: 1
        },
        {
          title: '2xBigFries',
          image:
            'https://dsmaipc1fg2ox.cloudfront.net/uploads/20160229221357/frytki_srednie.png?421',
          description: 'Today we have big fries for you for half the price',
          cols: 1,
          rows: 1
        },
        {
          title: '2xCola',
          image:
            'https://dsmaipc1fg2ox.cloudfront.net/uploads/20170511122153/cola-0-4.png?421',
          description: 'Buy more for less. -30% ss',
          cols: 1,
          rows: 1
        }
      ];
    })
  );

  constructor(private breakpointObserver: BreakpointObserver) { }
}
