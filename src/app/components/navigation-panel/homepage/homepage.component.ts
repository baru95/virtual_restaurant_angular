import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent {
  /** Based on the screen size, switch from standard to one column per row */
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          {
            title: 'Welcome to our restaurant!',
            image: '../../../assets/images/order.jpg',
            description: `The secret of the perfect taste of food lies in the process of its preparation.
          Every stage is important here. That's why we at Virtual Restaurant do everything from scratch.
          Every day, we prepare the meat manually: we cover them, fry them and grill them.`,
            cols: 2,
            rows: 1
          },
          {
            title: 'Open 24h',
            image: '../../../assets/images/open.jpg',
            description: 'We are available 24 hours a day, 7 days a week.',
            cols: 1,
            rows: 1
          },
          {
            title: 'Fresh products every day',
            image: '../../../assets/images/cooking.jpg',
            description: `The Virtual Restaurant offer in Poland includes a wide range of sandwiches,
          drinks and desserts, as well as toppings, such as fries and salads. Their
          diversity allows to compose a suitably balanced meal by people with
          different needs and preferences.`,
            cols: 1,
            rows: 2
          },
          {
            title: 'Contact',
            image: '../../../assets/images/contact.jpg',
            description: `If you have comments to order, go to the 'About-us' tab and contact us.`,
            button: true,
            cols: 1,
            rows: 1
          }
        ];
      }

      return [
        {
          title: 'Welcome to our restaurant!',
          image: '../../../assets/images/order.jpg',
          description: `The secret of the perfect taste of food lies in the process of its preparation.
          Every stage is important here. That's why we at Virtual Restaurant do everything from scratch.
          Every day, we prepare the meat manually: we cover them, fry them and grill them.`,
          cols: 2,
          rows: 1
        },
        {
          title: 'Open 24h',
          image: '../../../assets/images/open.jpg',
          description: 'We are available 24 hours a day, 7 days a week.',
          cols: 1,
          rows: 1
        },
        {
          title: 'Fresh products every day',
          image: '../../../assets/images/cooking.jpg',
          description: `The Virtual Restaurant offer in Poland includes a wide range of sandwiches,
          drinks and desserts, as well as toppings, such as fries and salads. Their
          diversity allows to compose a suitably balanced meal by people with
          different needs and preferences. `,
          cols: 1,
          rows: 2
        },
        {
          title: 'Contact',
          image: '../../../assets/images/contact.jpg',
          description: `If you have comments to order, go to the 'About-us' tab and contact us.`,
          button: true,
          cols: 1,
          rows: 1
        }
      ];
    })
  );

  constructor(private breakpointObserver: BreakpointObserver) {}
}
