import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/rest-api/services/product.service';
import { Product } from 'src/app/rest-api/models/product.model';

@Component({
  selector: 'app-sandwich-list',
  templateUrl: './sandwich-list.component.html',
  styleUrls: ['./sandwich-list.component.scss']
})
export class SandwichListComponent implements OnInit {
  title = 'Sandwiches';
  sandwiches: Product[];

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.getSandwiches();
  }

  getSandwiches() {
    this.productService.getProductsByCategory('Sandwiches').subscribe(
      product => (this.sandwiches = product));
  }
}
