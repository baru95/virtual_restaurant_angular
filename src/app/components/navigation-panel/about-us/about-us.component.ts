import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  title = 'About us';
  zoom: Number = 16;
  lat: Number = 50.069061;
  lng: Number = 19.904303;

  constructor() {}

  ngOnInit() {}
}
