import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/rest-api/models/product.model';
import { ProductService } from 'src/app/rest-api/services/product.service';

@Component({
  selector: 'app-fries-list',
  templateUrl: './fries-list.component.html',
  styleUrls: ['./fries-list.component.scss']
})
export class FriesListComponent implements OnInit {
  title = 'Fries';
  fries: Product[];

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.getFries();
  }

  getFries() {
    this.productService.getProductsByCategory('Fries').subscribe(
      product => (this.fries = product));
  }
}
