import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

export interface Transaction {
  item: string;
  cost: number;
}

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit {

  transactions: Transaction[] = [
    { item: 'Burger', cost: 4 },
    { item: 'Sandwich', cost: 5 },
    { item: 'Coca-cola', cost: 2 },
    { item: 'Water', cost: 4 },
    { item: 'Chicken salad', cost: 25 },
    { item: 'Curly fries', cost: 15 },
  ];

  displayedColumns: string[] = ['select', 'item', 'cost'];
  dataSource = new MatTableDataSource<Transaction>(this.transactions);
  selection = new SelectionModel<Transaction>(true, []);

  displayedColumns2: string[] = ['item', 'cost'];

  constructor() { }

  ngOnInit() {
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  getTotalCost() {
    return this.transactions.map(t => t.cost).reduce((acc, value) => acc + value, 0);
  }
}
