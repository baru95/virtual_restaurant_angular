import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/rest-api/models/product.model';
import { ProductService } from 'src/app/rest-api/services/product.service';

@Component({
  selector: 'app-drink-list',
  templateUrl: './drink-list.component.html',
  styleUrls: ['./drink-list.component.scss']
})
export class DrinkListComponent implements OnInit {
  title = 'Drinks';
  drinks: Product[];

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.getDrinks();
  }

  getDrinks() {
    this.productService.getProductsByCategory('Drinks').subscribe(
      product => (this.drinks = product));
  }
}
