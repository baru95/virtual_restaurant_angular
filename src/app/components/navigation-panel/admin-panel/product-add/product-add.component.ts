import { Component, ViewChild } from '@angular/core';
import { Product } from 'src/app/rest-api/models/product.model';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { ProductService } from 'src/app/rest-api/services/product.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.scss']
})
export class ProductAddComponent {

  @ViewChild('productForm') productForm: NgForm;
  categories = ['Sandwiches', 'Fries', 'Salads', 'Drinks'];
  message_added = 'Succesfully added product';
  message_edited = 'Succesfully edited product';
  action = 'Close';

  product: any = {};

  submitted = false;

  constructor(
    public snackBar: MatSnackBar,
    private productService: ProductService
  ) { }

  onSubmit() {
    this.productService.createProduct(this.product).subscribe(product => {
      this.openSnackBar(this.message_added, this.action);
    });
    this.submitted = true;
  }

  onEdit() {
    this.productService.modifyProduct(this.product).subscribe(product => {
      this.openSnackBar(this.message_edited, this.action);
    });
    this.submitted = false;
  }

  resetForm() {
    this.productForm.resetForm();
    this.submitted = false;
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.horizontalPosition = 'center';
    config.verticalPosition = 'top';
    config.duration = 5000;
    this.snackBar.open(message, action, config);
  }
}
