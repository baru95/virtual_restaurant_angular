import { Component, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { Product } from 'src/app/rest-api/models/product.model';
import { ProductService } from 'src/app/rest-api/services/product.service';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.scss']
})
export class AdminPanelComponent implements OnInit {
  title = 'Admin panel';
  message = 'Succesfully deleted product';
  action = 'Close';
  submitted = false;

  displayedColumns = [
    'id',
    'name',
    'category',
    'price',
    'action'
  ];

  ELEMENT_DATA: Product[];
  dataSource = this.ELEMENT_DATA;

  constructor(
    public snackBar: MatSnackBar,
    private productService: ProductService
  ) { }

  ngOnInit() {
    this.getProducts();
  }

  getProducts(): void {
    this.productService.getProducts().subscribe(products => {
      this.dataSource = products;
    });
  }

  deleteProduct(product: Product): void {
    this.dataSource = this.dataSource.filter(p => p !== product);
    this.productService.deleteProduct(product).subscribe(p => {
      this.openSnackBar(this.message, this.action);
    });
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.horizontalPosition = 'center';
    config.verticalPosition = 'top';
    config.duration = 5000;
    this.snackBar.open(message, action, config);
  }
}
