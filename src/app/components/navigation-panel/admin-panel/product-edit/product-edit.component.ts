import { Component, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { ProductService } from 'src/app/rest-api/services/product.service';
import { Product } from 'src/app/rest-api/models/product.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})
export class ProductEditComponent implements OnInit {
  product: Product;
  categories = ['Sandwiches', 'Fries', 'Salads', 'Drinks'];
  message = 'Succesfully edited product';
  action = 'Close';
  submitted = false;
  constructor(
    public snackBar: MatSnackBar,
    private productService: ProductService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.getProduct();
  }

  getProduct(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.productService.getProduct(id)
      .subscribe(product => this.product = product);
  }

  onSubmit() {
    this.productService.createProduct(this.product).subscribe(product => {
      this.openSnackBar(this.message, this.action);
    });
    this.submitted = true;
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.horizontalPosition = 'center';
    config.verticalPosition = 'top';
    config.duration = 4000;
    this.snackBar.open(message, action, config);
  }
}
