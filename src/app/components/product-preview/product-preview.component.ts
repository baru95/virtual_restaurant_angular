import { Component, Input } from '@angular/core';
import { Product } from 'src/app/rest-api/models/product.model';

@Component({
  selector: 'app-product-preview',
  templateUrl: './product-preview.component.html',
  styleUrls: ['./product-preview.component.scss']
})
export class ProductPreviewComponent {
  @Input()
  product: Product;
}
