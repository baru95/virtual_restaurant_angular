import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Product } from 'src/app/rest-api/models/product.model';
import { ProductService } from 'src/app/rest-api/services/product.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {
  title = 'Details';
  product: Product;

  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  getProduct() {
    const id = +this.route.snapshot.params['id'];
    this.productService.getProduct(id).subscribe(product => {
      this.product = product;
    });
  }

  goBack(): void {
    this.location.back();
  }

  ngOnInit() {
    this.getProduct();
  }
}
