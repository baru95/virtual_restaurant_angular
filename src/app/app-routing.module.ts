import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OktaCallbackComponent } from '@okta/okta-angular';

import { AboutUsComponent } from './components/navigation-panel/about-us/about-us.component';
import { AdminPanelComponent } from './components/navigation-panel/admin-panel/admin-panel.component';
import { ProductAddComponent } from './components/navigation-panel/admin-panel/product-add/product-add.component';
import { ProductEditComponent } from './components/navigation-panel/admin-panel/product-edit/product-edit.component';
import { DiscountComponent } from './components/navigation-panel/discount/discount.component';
import { DrinkListComponent } from './components/navigation-panel/drink-list/drink-list.component';
import { FriesListComponent } from './components/navigation-panel/fries-list/fries-list.component';
import { HomepageComponent } from './components/navigation-panel/homepage/homepage.component';
import { SaladListComponent } from './components/navigation-panel/salad-list/salad-list.component';
import { SandwichListComponent } from './components/navigation-panel/sandwich-list/sandwich-list.component';
import { ShoppingCartComponent } from './components/navigation-panel/shopping-cart/shopping-cart.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { NotFoundPageComponent } from './shared/components/not-found-page/not-found-page.component';

const routes: Routes = [
  { path: '', redirectTo: '/homepage', pathMatch: 'full' },
  { path: 'homepage', component: HomepageComponent },
  { path: 'about-us', component: AboutUsComponent },
  {
    path: 'products',
    children: [
      { path: '', component: ProductListComponent },
      { path: 'add', component: ProductAddComponent },
      { path: ':id', component: ProductDetailsComponent },
      { path: ':id/edit', component: ProductEditComponent }
    ]
  },
  { path: 'sandwiches', component: SandwichListComponent },
  { path: 'fries', component: FriesListComponent },
  { path: 'drinks', component: DrinkListComponent },
  { path: 'salads', component: SaladListComponent },
  { path: 'discount', component: DiscountComponent },
  { path: 'cart', component: ShoppingCartComponent },
  { path: 'admin-panel', component: AdminPanelComponent },
  { path: 'implicit/callback', component: OktaCallbackComponent },
  { path: '**', component: NotFoundPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
