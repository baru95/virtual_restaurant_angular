import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationPanelComponent } from './components/navigation-panel/navigation-panel.component';
import { LayoutModule } from '@angular/cdk/layout';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { AboutUsComponent } from './components/navigation-panel/about-us/about-us.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FooterComponent } from './components/footer/footer.component';
import { MaterialModule } from './shared/modules/material.module';
import { AgmCoreModule } from '@agm/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OktaAuthModule } from '@okta/okta-angular';

import { HomepageComponent } from './components/navigation-panel/homepage/homepage.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { DiscountComponent } from './components/navigation-panel/discount/discount.component';
import { ProductPreviewComponent } from './components/product-preview/product-preview.component';
import { AdminPanelComponent } from './components/navigation-panel/admin-panel/admin-panel.component';
import { ProductAddComponent } from './components/navigation-panel/admin-panel/product-add/product-add.component';
import { ProductEditComponent } from './components/navigation-panel/admin-panel/product-edit/product-edit.component';
import { SaladListComponent } from './components/navigation-panel/salad-list/salad-list.component';
import { DrinkListComponent } from './components/navigation-panel/drink-list/drink-list.component';
import { FriesListComponent } from './components/navigation-panel/fries-list/fries-list.component';
import { ShoppingCartComponent } from './components/navigation-panel/shopping-cart/shopping-cart.component';
import { SandwichListComponent } from './components/navigation-panel/sandwich-list/sandwich-list.component';
import { NotFoundPageComponent } from './shared/components/not-found-page/not-found-page.component';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';
import { AuthInterceptor } from './shared/interceptors/auth.interceptor';
import { IngredientAddComponent } from './components/navigation-panel/admin-panel/ingredient-add/ingredient-add.component';
import { IngredientEditComponent } from './components/navigation-panel/admin-panel/ingredient-edit/ingredient-edit.component';
import { DiscountAddComponent } from './components/navigation-panel/admin-panel/discount-add/discount-add.component';
import { DiscountEditComponent } from './components/navigation-panel/admin-panel/discount-edit/discount-edit.component';

const config = {
  issuer: 'https://dev-143000.oktapreview.com/oauth2/default',
  redirectUri: 'http://localhost:4200/implicit/callback',
  clientId: '0oaicvjldzoucpN7u0h7'
};

@NgModule({
  declarations: [
    AppComponent,
    NavigationPanelComponent,
    HomepageComponent,
    AboutUsComponent,
    ProductListComponent,
    FooterComponent,
    ProductDetailsComponent,
    AdminPanelComponent,
    ProductAddComponent,
    ProductEditComponent,
    DiscountComponent,
    ProductPreviewComponent,
    SaladListComponent,
    DrinkListComponent,
    FriesListComponent,
    ShoppingCartComponent,
    SandwichListComponent,
    NotFoundComponent,
    NotFoundPageComponent,
    IngredientAddComponent,
    IngredientEditComponent,
    DiscountAddComponent,
    DiscountEditComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    FlexLayoutModule,
    MaterialModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCgRZMhE_5KPjHn4Zkt05M4NGNx0toQhhg'
    }),
    FormsModule,
    ReactiveFormsModule,
    OktaAuthModule.initAuth(config)
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
