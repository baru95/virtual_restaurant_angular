export interface Ingredient {
    ingId: number;
    ingName: string;
    ingPrice: number;
    ingDescription: string;
    ingImageSrc: string;
}
