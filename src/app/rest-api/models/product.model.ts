import { NutritionalValues } from './nutritional-values.model';
import { Ingredient } from './ingredient.model';


export interface Product {
  id: number;
  name: string;
  category: string;
  price: number;
  description: string;
  imageSrc: string;
  nutritionalValues: NutritionalValues;
  ingredient: Ingredient;
}
