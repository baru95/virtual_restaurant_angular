export interface NutritionalValues {
    nutValId: number;
    nutValWeight: number;
    nutValCalories: number;
    nutValProtein: number;
    nutValCarbo: number;
    nutValSugar: number;
    nutValFats: number;
}
