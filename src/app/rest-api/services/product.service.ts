import { Injectable } from '@angular/core';
import { Product } from '../models/product.model';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private baseUrl = 'http://localhost:8080/products';

  constructor(private http: HttpClient) { }

  /**
   *@description Method returns Products
   *@param product body of modify object
   *@return http response object
   * */
  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.baseUrl);
  }

  /**
   *@description Method returns Product
   *@param product body of modify object
   *@return http response object
   * */
  getProduct(id: number): Observable<Product> {
    const productUrl = `${this.baseUrl}/${id}`;
    return this.http.get<Product>(productUrl);
  }

  /**
   *@description Method returns ProductByCategory
   *@return http response object
   * */
  getProductsByCategory(category: string): Observable<Product[]> {
    const options = category ? { params: new HttpParams().set('category', category + '') } : {};
    return this.http.get<Product[]>(this.baseUrl, options);
  }

  /**
   *@description Method delete Product
   *@param product body of modify object
   *@return http response object
   * */
  deleteProduct(product: Product | number): Observable<Product> {
    const id = typeof product === 'number' ? product : product.id;
    const productUrl = `${this.baseUrl}/${id}`;
    return this.http.delete<Product>(productUrl, httpOptions);
  }

  /**
   *@description Method created Product
   *@param product body of modify object
   *@return http response object
   * */
  createProduct(product: Product): Observable<Product> {
    return this.http.post<Product>(this.baseUrl, product, httpOptions);
  }

  /**
   *@description Method modify Product
   *@param product body of modify object
   *@return http response object
   * */
  /*modifyProduct(product: Product): Observable<Product> {
    return this.http.put<Product>(this.baseUrl, product, httpOptions);
  }*/

  modifyProduct(product: Product): Observable<Product> {
    return this.http.put<Product>(this.baseUrl + product.id, product);
  }
}
